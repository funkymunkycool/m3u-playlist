#!/usr/bin/python3
################################################################################

from re       import sub
from requests import get
from lxml     import html
from os       import linesep, popen, remove, system
import sys

def read_channel_search():
    '''
    Read the channel names from a file called 'search_terms.dat'. Each channel name should be a separate file. Returns the channels ready for use in a search.
    '''
    try:
        sterms=open('search_terms.dat','r').readlines()
    except IOError:
        print '\nINPUT ERROR!! search_terms.dat not found. You need to make one otherwise program will not run!'
        sys.exit()

    searchString=''
    if len(sterms) > 1:
        for term in sterms:
            searchString=searchString+'%28'+"+".join(term.split())+'%29+OR+'
        searchString=searchString[:-4]
    else:
        searchString="+".join(sterms[0].split())
    return searchString

def get_search_results( search_url ):
  # Get search URL create xml tree then form raw pastebin url
  #print search_url
  link_list     = []
  raw_link_list = []

  r    = get( search_url )
  tree = html.fromstring(r.text)

  for item in tree.xpath(".//*[@id='ires']/ol/div/div/div/cite"):
    raw_link_list.append(item.text)

  for link in raw_link_list:
    split_link = link.split(".com/")
    raw_link = ".com/raw/".join(split_link)
    link_list.append(raw_link)

  return link_list

def download_pastebin_result(link):
  # Return urls from EXTM3U file
  fileName = "m3u-output-raw.m3u"

  raw_file      = get(link).text
  processedFile = ""

  processedFile = linesep.join([s for s in raw_file.splitlines() if s])
  processedFile = processedFile.encode('utf-8')

  f = open( fileName , 'w')
  f.write(processedFile)
  f.close

  raw_urls = sub(r'(?m)^\#.*\n?', '', processedFile)
#  list_of_urls = raw_urls.split("\r\n")
  list_of_urls = raw_urls.split()
  return list_of_urls

def openOutput():
    try:
      remove('m3u-output.m3u')
    except OSError:
      pass
    fout = open('m3u-output.m3u','w')
    fout.write('#EXTM3U\n')
    return fout

def check_working_urls(list_of_urls,f):
  # Checks headers of urls to see if they are video stream
#  f = open('m3u-output.m3u', 'w')
#  f.write('#EXTM3U\n')
  for url in list_of_urls:
    try:
      response = get(url, stream=True, timeout=1)
      if response:
        channel_name = popen("grep -B 1 " + url + " m3u-output-raw.m3u" + \
                                                        " | head -n1").read()
        unformed_url = url.split(".")
        unformed_url.pop()
        unformed_url.append("m3u8")
        formed_url = ".".join(unformed_url)
        print("FOUND " + url)
        f.write(channel_name)
        f.write(str(formed_url + '\n'))
    except:
      print("Timedout " + url)
  remove('m3u-output-raw.m3u')
  f.close
  return None

def check_all_links(list_of_urls,fout):
  for pastebin_url in list_of_urls:
    url_list = download_pastebin_result(pastebin_url)
    print("Checking URLs, this may take some time...")
    separator()
    check_working_urls(url_list,fout)
    separator()
  return url_list

def add_comments_to_file(pastebin_urls):
  f  = open('m3u-output.m3u', 'a')
  f.write('# This file was generated by m3u-output.py\n')

  for pastebin_url in pastebin_urls:
    f.write('# ' + pastebin_url + "\n")

  f.close
  return None

def separator():
  print('#' * 80)
  return None

def main():
  separator()
  print("Starting...")

  # Forming the search URL
  searchString = read_channel_search()
  baseUrl      = "http://www.google.co.uk/search?"
  queryString  = "q=site:pastebin.com+%23EXTM3U+" + searchString
  timeCriteria = "&tbs=qdr:"
  finalSuffix = ",sbd:1&num=100"
  searchString = baseUrl + queryString + timeCriteria
  day          = "w"
  month        = "m"

  try:
    pastebin_urls = get_search_results( searchString + day + finalSuffix)
  except:
    try:
      pastebin_urls = get_search_results( searchString + month + finalSuffix)
    except:
      print("No valid URLs found, this program will exit")
      separator()
      exit()

  fout = openOutput()
  check_all_links(pastebin_urls,fout)
  fout.close()

  add_comments_to_file(pastebin_urls)
  print("URLs have been checked and are currently serving video content.")
  print("These have been written to m3u-output.m3u")
  separator()

if __name__ == '__main__':
    main()
